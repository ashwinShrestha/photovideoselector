//
//  ViewController.swift
//  PhotoVideoSelector
//
//  Created by Ashwin Shrestha on 4/6/17.
//  Copyright © 2017 Ashwin Shrestha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func addImageBtnClick(_ sender: UIButton) {
        
        // you need to put few description in plist file like
        //photolibraryusage, camera usage, microphone usage
        
        
        let imagePicker = ImagePickerViewController.ImagePicker() // returns uinavigation controller and imagepicker view controller
        
        imagePicker.1.delegate = self
        imagePicker.1.pickerMode = .library
        imagePicker.1.pickerType = .photo
        imagePicker.1.croppingEnabled = true
        imagePicker.1.croppingStyle = .default
        imagePicker.1.aspectRatioPreset = .preset4x3
        imagePicker.1.aspectRatioLockEnabled = true
        imagePicker.1.resetAspectRatioEnabled = false
        imagePicker.1.rotateButtonsHidden = true
        
        imagePicker.0.modalTransitionStyle = .crossDissolve
        imagePicker.0.modalPresentationStyle = .custom
        
        self.present(imagePicker.0, animated: false, completion: nil)
        
        
    }
    
}

//MARK: ImagePicker Methods
extension ViewController : ImagePickerProtocol {
    
    func didPick(image: UIImage) {
        
    }
    
    func didPickVideo(withPath: URL) {
        
        print(withPath)
    }
}
