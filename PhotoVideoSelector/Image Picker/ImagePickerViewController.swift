//
//  ImagePickerViewController.swift
//  CruvaCustomer
//
//  Created by IosDeveloper on 12/19/16.
//  Copyright © 2016 Sayami. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices


enum PickerMode {
    case all,camera, library
}

enum PickerType {
    case video, photo
}

protocol ImagePickerProtocol : class {
    
    func didPick(image: UIImage)
    func didPickVideo(withPath : URL)
    
}

class ImagePickerViewController: UIViewController {

    
    var delegate: ImagePickerProtocol?
    var pickerMode: PickerMode  = .all
    var pickerType : PickerType = .photo
    
    var hasCameraAccess = false
    var hasLibraryAccess = false
    var imagePicker = UIImagePickerController()
    
    var imageCropper: TOCropViewController?
    
    var aspectRatioPreset: TOCropViewControllerAspectRatioPreset = .preset4x3
    var aspectRatioLockEnabled: Bool = true
    var resetAspectRatioEnabled: Bool = true
    var rotateButtonsHidden: Bool = true
    var croppingStyle: TOCropViewCroppingStyle = .default
    
    var croppingEnabled = false
    var sender : UIButton!
    
    
    
    class func ImagePicker()-> (UINavigationController,ImagePickerViewController) {
        
        let imagePicker = ImagePickerViewController(nibName: "ImagePickerViewController", bundle: nil)
        let nav = UINavigationController(rootViewController: imagePicker)
        nav.modalPresentationStyle = .custom
        nav.modalTransitionStyle = .crossDissolve
        nav.setNavigationBarHidden(true, animated: false)
        return (nav,imagePicker)
        
    }
    
    func checkIfPermisionInPlist() -> Bool {
    
        var dictRoot: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info" , ofType: "plist") {
            dictRoot = NSDictionary(contentsOfFile: path)
        }
        
        if let _ = dictRoot?["NSPhotoLibraryUsageDescription"] as? String , let _ = dictRoot?["NSCameraUsageDescription"] as? String {
            return true
        }
        
        return false
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !checkIfPermisionInPlist() {
            self.showAlertWithOkHandler(message: "There seems to be no permission in info plist file for both photo and camera access.", okHandler: {
                self.dismiss(animated: true, completion: nil)
            })
        }
        else {
            hasCameraAccess = UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            hasLibraryAccess = UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            if pickerMode == .all {
                if hasCameraAccess && hasLibraryAccess {
                    
                    
                    self.showActionSheet()
                }
                else{
                    showFromModeAvailable()
                }
            }
            else{
                showFromSelectMode()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        
    }
    
    
    @IBAction func viewTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showFromSelectMode () {
        
        if pickerMode == .camera {
            if !hasCameraAccess {
                self.showAlertWithOkHandler(message: "No camera available.", okHandler: {
                    self.dismiss(animated: true, completion: nil)
                })
                return
            }
            self.mediaFromCamera()
        }
        else {
            
            if !hasLibraryAccess {
                self.showAlertWithOkHandler(message: "No \(pickerType == .video ? "video": "photo") library available.", okHandler: {
                    self.dismiss(animated: true, completion: nil)
                })
                return
            }
            self.mediaFromGallery()
        }
    }
    
    func showFromModeAvailable() {
        
        if hasCameraAccess {
            mediaFromCamera()
        }
        else if hasLibraryAccess {
            mediaFromGallery()
        }
        else{
            self.showAlertWithOkHandler(message: "No access type available to select \(pickerType == .video ? "video" : "photo")", okHandler: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    func showActionSheet() {
        
        let actionSheet = UIAlertController(title: "Choose option", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: {
                handler in
                
            self.mediaFromGallery()
                
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {
            handler in
            
            self.mediaFromCamera()
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            cancel in
            self.dismiss(animated: true, completion: nil)
        }))
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.permittedArrowDirections = .up
        }
        
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func mediaFromCamera(){
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized
        {
            
            
            imagePicker.sourceType = .camera
            if pickerType == .video {
                imagePicker.mediaTypes = [kUTTypeVideo as String, kUTTypeMovie as String]
            }
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        else
        {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    
                    self.imagePicker.sourceType = .camera
                    if self.pickerType == .video {
                        self.imagePicker.mediaTypes = [kUTTypeVideo as String, kUTTypeMovie as String]
                    }
                    self .present(self.imagePicker, animated: true, completion: nil)
                }
                else
                {
                    self.showAlertOnMainThread(message: "App needs camera permission to capture picture")
                    
                }
            });
        }
       
    }
    
    func mediaFromGallery(){
        
        imagePicker.sourceType = .savedPhotosAlbum
        
        if pickerType == .video {
            imagePicker.mediaTypes = [kUTTypeVideo as String, kUTTypeMovie as String]
        }
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    
}


extension ImagePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if pickerType == .photo {
            
            if let  image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                if croppingEnabled {
                    
                    if let _ = imageCropper {}
                    else {
                        imageCropper = TOCropViewController(croppingStyle: self.croppingStyle, image: image)
                    }
                    imageCropper?.delegate = self
                    imageCropper?.aspectRatioLockEnabled = aspectRatioLockEnabled
                    imageCropper?.aspectRatioPreset = aspectRatioPreset
                    imageCropper?.resetAspectRatioEnabled = resetAspectRatioEnabled
                    imageCropper?.rotateButtonsHidden = rotateButtonsHidden
                    
                    self.dismiss(animated: true, completion: {
                        
                        self.present(self.imageCropper!, animated: true, completion: nil)
                    })
                }
                else {
                    self.delegate?.didPick(image: image)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
       }
        else {
            
            if let url = info[UIImagePickerControllerMediaURL] as? URL {
                self.delegate?.didPickVideo(withPath: url)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

}

extension ImagePickerViewController: TOCropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController!, didCropTo image: UIImage!, with cropRect: CGRect, angle: Int) {
        
        self.dismiss(animated: true) {
            self.delegate?.didPick(image: image)
        }
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController!, didCropToCircularImage image: UIImage!, with cropRect: CGRect, angle: Int) {
        
        self.dismiss(animated: true) {
            self.delegate?.didPick(image: image)
        }
    }
    
}
