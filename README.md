# README #

This is a simple try and made custom viewcontroller class which helps user selects video or photo, and picker type (camera, photo library or both)

### What is this repository for? ###

* Help user select photo / video depending upon the access given for media access. 

### How do I get set up? ###


    This is a demo app, which illustrates the use of this custom view controller. 


    // on click of button to add image or video
    @IBAction func addImageBtnClick(_ sender: UIButton) {
        
        // you need to put few description in plist file like
        //photolibraryusage, camera usage, microphone usage
        
        
        let imagePicker = ImagePickerViewController.ImagePicker() // returns uinavigation controller and imagepicker view controller
        
        imagePicker.1.delegate = self
        imagePicker.sender = sender // for ipad popovercontroller
        imagePicker.1.pickerMode = .library        picker modes available (.library, .camera, .all)
        imagePicker.1.pickerType = .photo          picker type available (.photo, .video)

        // use if pickerType is photo 
        imagePicker.1.croppingEnabled = true      // Croping when enabled, uses custom library downloaded to crop the image

        // use if cropping enabled
        imagePicker.1.croppingStyle = .default
        imagePicker.1.aspectRatioPreset = .preset4x3
        imagePicker.1.aspectRatioLockEnabled = true
        imagePicker.1.resetAspectRatioEnabled = false
        imagePicker.1.rotateButtonsHidden = true
        

        imagePicker.0.modalTransitionStyle = .crossDissolve
        imagePicker.0.modalPresentationStyle = .custom
        
        self.present(imagePicker.0, animated: false, completion: nil)
        
        
    }



# On delegate maintained to retreive selected image or video


    extension ViewController : ImagePickerProtocol {
    
        func didPick(image: UIImage) {
        
        }
    
        func didPickVideo(withPath: URL) {
        
            print(withPath)
        }
    }